//猫公式工程。 写代码套公式，已有printf及scanf / 循环 / 分支 / 函数。
//另外，遇到没见过的东西或者不明白的东西，找百度。比如printf不明白，搜c printf。

#include<stdio.h>  //#include<> 用来加载头文件，<>中为头文件名。该头文件代表外部库。
//比如你需要用到输出输入printf/scanf函数时，他们是外部库提供的stdio.h，就#include<stdio.h>。

int max2(int a, int b)  //max2函数的实现
{
	int m = 0;
	if (a > b) {
		m = a;
	}
	else {
		m = b;
	}
	return m;
}

int main(void)
{
	int i = 0;
	int j = 1;
	int k = 0;
	float b = 0.0f;//float类型的数字后面跟f，是规定。
	
	//函数两种使用方式。第一种，先声明，再调用，实现放后面。如max
	int max(int a, int b);  //声明函数。这里是声明max函数，是告诉后面调用的地方已经有max函数。不声明，后面会报错。其实现放到main函数后
	k = max(i, j);  //调用函数。 这里是调用max函数来求i和j的最大值，输入i,j给该函数，函数计算后结果返回赋值给k
	//函数第二种，实现放前面，不用声明，直接调用。如max2
	k = max2(i, j);

	printf("打印文本\n"); //输出函数。 双引号中的任何东西（除部分保留符如%d）都会被认为是文本,\n是换行。
	printf("输出整数变量i %d和两位小数b %0.2f， \n", i, b); //首先给个%，告诉编译器我要输出某类型数据， d、lf、0.xf用来指明具体是哪种数据。凡输出所用变量需用, 分隔跟在括号内双引号后。
	scanf("%d %f", &i, &b); //用户输入一个整数给i，输入一个小数给b。记得后面每个变量前加上&。为什么暂时无需了解

	////////分支有两种。
		//分支第一种：if/else if/else例子，如果i = 1, 输出1，如果 i = 2输出2，否则输出other。
	if (i == 1)//这里注意呀，==表示判断两数是否相等，=表示把等数后面的值赋给等号前面的变量。 
	{
		printf("1");
	}
	else if (i == 2)
	{
		printf("2");
	}
	else
	{
		printf("other\n");
	}
	//分支第一种：swithc例子
	switch (i) {
	case 1: b = 1; break;
	case 2: b = 2; break;
	case 3: b = 4; break;
	case 4: b = 8; break;
	default: b = 0.0; break;
	}
	printf("b = %d\n", b);

	/////////////循环有3种
	//第一种：for例子：如：i的初始值为0，, 每当i<4, 那么在屏幕上打出一行123，, 同时i + 1.
	for (i = 0; i < 4; i++)
	{
		printf("for4\n");
	}

	//第二种： wihle例：如果i>1, 输出while，并使i - 1 
	while (i > 1)
	{
		printf("while\n");
		i = i - 1;
	}

	//第三种：do while例：不管i等于几，先在屏幕让输出123, 并使i - 1，如果a>5那么继续这些。
	do
	{
		printf("do while\n");
		i = i - 1;
	} while (i > 5);

	return 0;
}

int max(int a, int b)  //max函数的实现
{
	int m = 0;
	if (a > b) {
		m = a;
	}
	else {
		m = b;
	}
	return m;
}